import { Component, OnInit, Input } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';
import { HostBinding } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {
@Input() destino: DestinoViaje;
@HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() {}

  ngOnInit() {
  }

}
