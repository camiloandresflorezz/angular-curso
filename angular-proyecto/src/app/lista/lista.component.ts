import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
destinos: DestinoViaje[];
  constructor() {
  this.destinos= []; 
  }

  ngOnInit() {
  }
guardar(nombre:string, url:string):boolean{
this.destinos.push(new DestinoViaje(nombre, url));
	console.log(this.destinos);
	return false;
}
}
